<?php
/*
 * Localization file for ident_switch plugin
 */
$labels = array();

// IMAP
$labels['form.caption'] = 'IMAP';

// Enabled
$labels['form.enabled'] = 'Enabled';

// Label
$labels['form.label'] = 'Label';

// Server host name
$labels['form.host']='Server host name';

// Secure connection (SSL/TLS)
$labels['form.secure']='Connection security';

// Port
$labels['form.port']='Port';

// Username
$labels['form.username'] = 'Username';

// Password
$labels['form.password'] = 'Password';

// Folder hierarchy delimiter
$labels['form.delimiter'] = 'Folder hierarchy delimiter';

// Value in \'Label\' field of IMAP section is too long (32 chars max).
$labels['err.label.long'] = 'Value in \'Label\' field of IMAP section is too long (32 chars max).';

// Value in \'Server host name\' field of IMAP section is too long (64 chars max).
$labels['err.host.long'] = 'Value in \'Server host name\' field of IMAP section is too long (64 chars max).';

// Value in \'Username\' field of IMAP section is too long (64 chars max).
$labels['err.user.long'] = 'Value in \'Username\' field of IMAP section is too long (64 chars max).';

// Value in \'Folder hierarchy delimiter\' field of IMAP section is too long (1 char max).
$labels['err.delim.long'] = 'Value in \'Folder hierarchy delimiter\' field of IMAP section is too long (1 char max).';

// Value in \'Port\' field of IMAP section must be a number.
$labels['err.port.num'] = 'Value in \'Port\' field of IMAP section must be a number.';

// Value in \'Port\' field of IMAP section must be between 1 and 65535.
$labels['err.port.range'] = 'Value in \'Port\' field of IMAP section must be between 1 and 65535.';
