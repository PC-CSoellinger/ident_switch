# ident_switch
ident_switch plugin for Roundcube

This plugin allows users to switch between different accounts (including remote) in single Roundcube session.

*Inspired by identities_imap plugin that is no longer supported.*

### Where to start ###
* In settigs interface create new identity.
* For all identities except default you will see new section of settings - IMAP. Enter data required to connect to  remote server. Don't forget to check Enabled check box.
* After you have created at least one identity with active IMAP settings you will see combobox in the top right corner instead of plain text field with account name. It will allows you to switch to another account.